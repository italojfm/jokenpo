/***************************************************************************
 *   jokenpo.c                                Version 20190906.163244      *
 *                                                                         *
 *   Pedra Papel ou Tesoura                                                *
 *   Copyright (C) 2019                                                    *
 ***************************************************************************
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License.               *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
 *   To contact the author, please write to:                               *
 *   Italo Jamison Felix Monteiro                                          *
 *   Email: italojamison@gmail.com                                         *
 *   Webpage: http://beco.poli.br/italojamison@gmail.com                                         username                                *
 *   Phone: (81) 98327-8817                                                *
 ***************************************************************************/

/*
 * Instrucoes para compilar:
 * $gcc jokenpo.c -o jokenpo.x -Wall -Wextra -g -O0 -DDEBUG=1
 * opcoes extras: -ansi -pedantic-errors
 */

/* ---------------------------------------------------------------------- */
/* includes */

#include <stdio.h> /* Standard I/O functions */
#include <stdlib.h> /* Miscellaneous functions (rand, malloc, srand)*/
/* #include "jokenpo.h" */ /* To be created for this template if needed */

/* ---------------------------------------------------------------------- */
/* prototypes */

/* ---------------------------------------------------------------------- */

int main(void)
{
    /* ...and we are done */
    return EXIT_SUCCESS;
}

/* ---------------------------------------------------------------------- */
/* add more functions here */


/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* vi: set ai et ts=4 sw=4 tw=0 wm=0 fo=croql : C config for Vim modeline */
/* Template by Dr. Beco <rcb at beco dot cc> Version 20160612.142044      */

